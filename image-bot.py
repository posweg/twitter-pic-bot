import tweepy #Twitter API
import pathlib #Read image's name and path
import os #See number of files in the folder
import time #Time between tweets

#Directory where the images are located
path = 'C:\\[insert path here]\\images'

#Authenticate OAuth
consumer_token = '[Insert consumer token here]'
consumer_secret = '[Insert consumer secret here]'
key = '[Insert key here]'
secret = '[Insert secret here]'

auth = tweepy.OAuthHandler(consumer_token, consumer_secret)
auth.set_access_token(key, secret)

api = tweepy.API(auth) 

#initialize the array to store names
flist = []

#main program loop, repeat until there are no images left on the directory, x is the actual image
for x in range(1, sum([len(files) for r, d, files in os.walk(path)])+1):
	#Read all the images in the folder and store the names and path in an array
	for p in pathlib.Path(path).iterdir():
		if p.is_file():
			flist.append(p)
	print (flist[x-1]) #Print in the console the image name with the path
	#If you want to tweet some text uncomment the line below, str(x) is the image number
	#status = 'Test #' + str(x) + ': '
	api.update_with_media(str(flist[x-1]), status=status)
	time.sleep(3) #time between images in seconds. 1 hour = 3600, 1/2 hours 1800
